#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <deque>
#include <fstream>

int xSpeed = 0;
int ySpeed = 0;
int speed = 20;
float xCoord = 21;
float yCoord = 21;
int score = 0;
int appleX;
int appleY;
bool dead = false;
sf::Color snakeColor = sf::Color(249,170,190,255);
sf::Color bgColor = sf::Color(255,240,240,255);

std::string scorehalf = "Score: ";
std::string scorestring = "Score: 0";


sf::RectangleShape apple(sf::Vector2f(15.0f, 15.0f));

std::deque<int> snakePartsX;
std::deque<int> snakePartsY;

sf::Font font;
sf::Text scoretext;
sf::Text dietext;

void makeAppleCoords() {
	appleX = (rand() % 40)*20 + 1 + 2;
	appleY = (rand() % 30)*20 + 1 + 2;
	apple.setPosition(appleX,appleY);
}

void eatApple() {
	score++;
	scorestring = scorehalf + std::to_string(score);
	scoretext.setString(scorestring);
	makeAppleCoords();
	snakePartsX.push_front(xCoord);
	snakePartsY.push_front(yCoord);
}

void die() {
	dead = true;
	speed = 0;
}

int main() 
{
sf::RenderWindow App(sf::VideoMode(801, 670, 32), "SFML Graphics");
    
App.setPosition(sf::Vector2i(560, 240));

font.loadFromFile("aAlleyGarden.ttf");

scoretext.setFont(font);
scoretext.setString("Score: 0");
scoretext.setCharacterSize(40);
scoretext.setFillColor(snakeColor);
scoretext.setPosition(15,610);

dietext.setFont(font);
dietext.setString("Dead");
dietext.setCharacterSize(150);
dietext.setFillColor(sf::Color(206,95,137,255));
dietext.setPosition(270,210);

App.setFramerateLimit(7);
    
sf::RectangleShape snakeHead(sf::Vector2f(19.0f,19.0f));
snakeHead.setFillColor(snakeColor);
snakeHead.setPosition(21,21);

sf::RectangleShape tailPart(sf::Vector2f(19.0f,19.0f));
tailPart.setFillColor(snakeColor);

apple.setFillColor(sf::Color(70,226,130,255));
makeAppleCoords();

sf::RectangleShape line(sf::Vector2f(801,2));
line.setFillColor(snakeColor);
line.setPosition(0,601);


snakePartsX.push_back(xCoord);
snakePartsY.push_back(yCoord);

while (App.isOpen())
    	{
        	sf::Event event;
        	while (App.pollEvent(event))
        	{
            	if (event.type == sf::Event::Closed)
                	App.close();
        	}

        App.clear(bgColor);
	
	snakePartsX.erase(snakePartsX.begin());
	snakePartsY.erase(snakePartsY.begin());
	
	snakePartsX.push_back(xCoord);
	snakePartsY.push_back(yCoord);
	
        App.draw(snakeHead); App.draw(apple); App.draw(scoretext); App.draw(line);
	for (int i = 0; i < score; i++) {
  		tailPart.setPosition(snakePartsX[i],snakePartsY[i]);
		if ((snakePartsX[i] == xCoord) && (snakePartsY[i] == yCoord)) {
			die();
		}
		App.draw(tailPart);
	}
	if (dead) {
		App.clear(sf::Color(255,240,240,255));
		App.draw(dietext);
		App.draw(scoretext);
		App.draw(line);
	}
        App.display();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D )) {
		if (xSpeed != -speed) { xSpeed = speed; ySpeed = 0; }
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A )) {
		if (xSpeed != speed) { xSpeed = -speed; ySpeed = 0; }
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S )) {
		if (ySpeed != -speed ) { ySpeed = speed; xSpeed = 0; }
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W )) {
		if (ySpeed != speed) { ySpeed = -speed; xSpeed = 0; }
	}
	
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape)) { App.close(); }
	
	if ((xCoord == appleX-2) && (yCoord == appleY-2)) { eatApple();}
	
	snakeHead.move(xSpeed,ySpeed);
	xCoord += xSpeed;
	yCoord += ySpeed;
	if (xCoord > 800) { die(); }
	if (yCoord > 600) { die(); }
	if (xCoord < 1) { die(); }
	if (yCoord < 1) { die(); }
}

return 0;
}
